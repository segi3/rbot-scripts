using RBot;
using System.Windows.Forms;

public class Script {

	private const int COUNT = 1;
	
	int iterations = 0;

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Player.LoadBank();
		
		bot.Skills.StartTimer();
        
        while(!bot.ShouldExit()){
        	
	        DefeatedMakai(bot, COUNT * 50);
	        bot.Player.Jump("Enter", "Spawn");
	        bot.Sleep(2000);
	        EscherionChain(bot, COUNT);
	        bot.Player.Jump("Portal", "Right");
	        bot.Log("Escherion's Chain: Done");
	        bot.Sleep(2000);
	        OdokuroTooth(bot, COUNT);
	        bot.Player.Jump("Enter", "Spawn");
            bot.Log("O-Dokuru's Tooth: Done");
	        bot.Sleep(2000);
	        VathHair(bot, COUNT);
	        bot.Player.Jump("Quest", "Left");
            bot.Log("Vath's Hair: Done");
	        bot.Sleep(2000);
	        AracaraFang(bot, COUNT);
	        bot.Player.Jump("Enter", "Spawn");
	        bot.Log("Aracara's Fang: Done");
	        bot.Sleep(2000);
	        HydraScale(bot, COUNT);
	        bot.Player.Jump("Enter", "Spawn");
	        bot.Log("Hydra Scale: Done");
	        bot.Sleep(2000);
	        
	        bot.Bank.ToInventory("Defeated Makai");
	        bot.Bank.ToInventory("Escherion's Chain");
	        bot.Bank.ToInventory("O-dokuro's Tooth");
	        bot.Bank.ToInventory("Strand of Vath's Hair");
	        bot.Bank.ToInventory("Aracara's Fang");
	        bot.Bank.ToInventory("Hydra Scale");
	        
	        for(int i = 0; i < COUNT; i++){
        		bot.Quests.EnsureAccept(570);
        		
        		if(bot.Map.Name != "djinn")
        			bot.Player.Join("djinn", "r5", "Center");
        		else
        			bot.Player.Jump("r5", "Center");
        		
        		while(!bot.Inventory.ContainsTempItem("Tibicenas' Chain")){
        			bot.Player.Kill("Tibicenas");
        		}
        		
        		bot.Player.Jump("Enter", "Right");
        		
		        bot.Quests.EnsureComplete(570);
		        	
		        bot.Wait.ForDrop("Dark Crystal Shard", 10);
		       	bot.Bank.StackGlitch("Dark Crystal Shard");
	        }
	        iterations++;
	        Application.OpenForms[0].Text = "RBot - Done " + iterations;
        }
	}
	
	public void DefeatedMakai(ScriptInterface bot, int count){
		if(bot.Bank.Contains("Defeated Makai", count))
			return;
		bot.Inventory.BankAllCoinItems();
		if(bot.Map.Name != "tercessuinotlim"){
	        bot.Player.Join("citadel", "m22", "Right");
            bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
        }
        bot.Player.Jump("m2", "Center");
        if(count <= 250)
            bot.Bank.ToInventory("Defeated Makai");
        
        bot.Player.SetSpawnPoint();
        
        int room = 1;
        while(!bot.Bank.Contains("Defeated Makai", count) && !bot.Inventory.Contains("Defeated Makai", 50)){
            bot.Player.Kill("Dark Makai");
            bot.Player.Kill("Dark Makai");
            if(bot.Player.DropExists("Defeated Makai")){
                if(count > 250)
                    bot.Bank.ToInventory("Defeated Makai");
                bot.Player.Pickup("Defeated Makai");
                if(count > 250)
                    bot.Inventory.ToBank("Defeated Makai");
            }
            if(bot.Player.DropExists("Essence of Nulgath"))
           		bot.Bank.StackGlitch("Essence of Nulgath");
            if(bot.Player.DropExists("Tendurrr the Assistant"))
            	bot.Bank.StackGlitch("Tendurrr the Assistant");
            bot.Player.Jump("m" + room, "Center");
            bot.Player.RejectExcept("Defeated Makai", "Essence of Nulgath", "Tendurrr the Assistant");
            room++;
            if(room >= 3)
                room = 1;
        }
	}
	
	public void EscherionChain(ScriptInterface bot, int count){
		if(bot.Bank.Contains("Escherion's Chain", count))
			return;
		bot.Player.Join("escherion", "Boss", "Left");
		bot.Player.SetSpawnPoint();
		bot.Inventory.BankAllCoinItems();
		
		while(!bot.Bank.Contains("Escherion's Chain", count)){
			bot.Player.Kill("Escherion");
			bot.Bank.StackGlitch("Escherion's Chain");
			bot.Player.RejectExcept("Escherion's Chain");
		}
	}
	
	public void OdokuroTooth(ScriptInterface bot, int count){
		if(bot.Bank.Contains("O-dokuro's Tooth", count))
			return;
		bot.Player.Join("yokaiwar", "Boss", "Left");
		bot.Player.SetSpawnPoint();
		bot.Inventory.BankAllCoinItems();
		
		while(!bot.Bank.Contains("O-dokuro's Tooth", count)){
			bot.Player.Kill("O-Dokuro's Head");
			bot.Bank.StackGlitch("O-dokuro's Tooth");
			bot.Player.RejectExcept("O-dokuro's Tooth");
		}
	}
	
	public void VathHair(ScriptInterface bot, int count){
		if(bot.Bank.Contains("Strand of Vath's Hair", count))
			return;
		bot.Player.Join("stalagbite", "r2", "Center");
		bot.Player.SetSpawnPoint();
		bot.Inventory.BankAllCoinItems();
		
		while(!bot.Bank.Contains("Strand of Vath's Hair", count)){
			bot.Player.Kill("Vath");
			if(bot.Monsters.Exists("Stalagbite") && !bot.Player.DropExists("Strand of Vath's Hair"))
				bot.Player.Kill("Stalagbite");
			bot.Bank.StackGlitch("Strand of Vath's Hair");
			bot.Player.RejectExcept("Strand of Vath's Hair");
		}
	}
	
	public void AracaraFang(ScriptInterface bot, int count){
		if(bot.Bank.Contains("Aracara's Fang", count))
			return;
		bot.Player.Join("faerie", "TopRock", "Center");
		bot.Player.SetSpawnPoint();
		bot.Inventory.BankAllCoinItems();
		
		while(!bot.Bank.Contains("Aracara's Fang", count)){
			bot.Player.Kill("Aracara");
			bot.Bank.StackGlitch("Aracara's Fang");
			bot.Player.RejectExcept("Aracara's Fang");
		}
	}
	
	public void HydraScale(ScriptInterface bot, int count){
		if(bot.Bank.Contains("Hydra Scale", count))
			return;
		bot.Player.Join("hydra", "Boss", "Left");
		bot.Player.SetSpawnPoint();
		bot.Inventory.BankAllCoinItems();
		
		while(!bot.Bank.Contains("Hydra Scale", count)){
			bot.Player.Kill("Hydra Head");
			bot.Bank.StackGlitch("Hydra Scale");
			bot.Player.RejectExcept("Hydra Scale");
		}
	}
}
