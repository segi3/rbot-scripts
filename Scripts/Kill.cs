using RBot;
using System;

public class Script{

	public void ScriptMain(ScriptInterface bot){

		bot.Events.PlayerDeath += (a) => {
            a.Schedule(11000, (b) => {
                b.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
		
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		//bot.Skills.StartSkills("Skills/timekeeper.xml");
		//bot.Player.EquipItem(62123);
		
		// while(!bot.ShouldExit()){
			bot.Player.Attack("*");
			bot.Wait.ForMonsterDeath();
			bot.Sleep(200);
		// }
	}
}