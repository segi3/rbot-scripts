using RBot;
using System;

// timekeeper 62123
// ssg 45363
// abbysal angel 45831
// revenant 48571

public class Script{

    public static readonly string [] DROPS = {
        "Corrupt Spirit Orb", "Dark Spirit Orb", "Doom Aura", "Ominous Aura"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "dwarfhold")
            bot.Player.Join("dwarfhold-1e99", "r2", "Left");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Left");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/LegionRevenant.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(48571);

        while(!bot.Inventory.Contains("Doom Aura", 1)) {

            bot.Quests.EnsureAccept(2185);

            bot.Player.Attack("*");
			bot.Sleep(200);

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(2185)) 
                bot.Quests.EnsureComplete(2185);
                bot.Sleep(2000);
            
        }
        
	}
}