using RBot;
using System.Windows.Forms;

public class Script {

	public class Test{
		public int a = 0;
		
		public Test(){
			a = 100;
		}
	}

	public void ScriptMain(ScriptInterface bot){
		Test a = new Test();
		bot.Log(a.a.ToString());
		MessageBox.Show(a.a.ToString());
	}
}