using RBot;
using System.Linq;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Skills.StartTimer();
		
		bot.Player.LoadBank();
		bot.Inventory.BankAllCoinItems();
		
		bot.Bank.ToInventory("Essence of Nulgath");
		bot.Bank.ToInventory("Voucher of Nulgath (non-mem)");
		bot.Bank.ToInventory("Totem of Nulgath");
		//4778
		
		if(bot.Map.Name != "tercessuinotlim"){
	        bot.Player.Join("citadel", "m22", "Right");
            bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
        }
        bot.RegisterHandler(10, (a) => {
			int aliveCount = bot.Monsters.CurrentMonsters.Sum(x => x.HP > 0 ? 1 : 0);
			if(aliveCount == 0){
				bot.Wait.ForCombatExit();
				if(bot.Player.Cell == "m2")
					bot.Player.Jump("m1", "Right");
				else
					bot.Player.Jump("m2", "Center");
			}
		});
        bot.Player.Jump("m2", "Center");
        bot.Player.SetSpawnPoint();		
		while(!bot.ShouldExit() && !bot.Inventory.Contains("Totem of Nulgath", 30)){
			bot.Quests.EnsureAccept(4778);
			
			bot.Player.Jump("m2", "Center");
			bot.Player.KillForItem("Dark Makai", "Essence of Nulgath", 60, false, true);
			
			bot.Player.Jump("Enter", "Spawn");
			bot.Quests.EnsureComplete(4778, 5357);
			
			bot.Wait.ForPickup("Totem of Nulgath");
			bot.Player.Pickup("Totem of Nulgath");
		}
	}
}
