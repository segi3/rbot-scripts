using RBot;
using System;

// timekeeper 62123
// ssg 45363
// abbysal angel 45831
// revenant 48571

public class Script{

    public static readonly string [] DROPS = {
        "Legion Token"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "fotia")
            bot.Player.Join("fotia-1e99", "Enter", "Right");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Right");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/LegionRevenant.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(48571);

        while(!bot.Inventory.Contains("Legion Token", 25000)) {

            bot.Quests.EnsureAccept(5755);

            bot.Player.Attack("*");
			bot.Sleep(200);

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(5755)) 
                bot.Quests.EnsureComplete(5755);
                bot.Sleep(2000);
            
        }
        
	}
}