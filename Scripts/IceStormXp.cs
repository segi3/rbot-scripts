using RBot;
using System;

public class Script{

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

		bot.Options.PrivateRooms = true;
		bot.Options.SafeRelogin = true;
        bot.Options.AutoReloginAny = true;

		bot.Player.Join("icestormarena");
		
		while(!bot.ShouldExit()){
			bot.Player.Hunt("Frost Spirit");
		}
	}
}