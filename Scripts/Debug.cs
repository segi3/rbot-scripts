using RBot;
using System.Threading;
using System.Windows.Forms;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Events.PlayerDeath += (a) => { ScriptManager.RestartScript(); };
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Skills.StartTimer();
		
		bot.Player.LoadBank();
		
		bot.Inventory.BankAllCoinItems();
		
		bot.Bank.ToInventory("Fragment of Chaos");
		bot.Bank.ToInventory("Essence of Nulgath");
		bot.Bank.ToInventory("Archfiend's Favor");
		bot.Bank.ToInventory("Blood Gem of the Archfiend");
		
		while(!bot.ShouldExit()){
			bot.Quests.EnsureAccept(3743);
			
			if(!bot.Inventory.Contains("Fragment of Chaos", 80)){
				if(bot.Map.Name != "northlands")
					bot.Player.Join("northlands", "r2", "Left");
				
				while(!bot.Inventory.Contains("Fragment of Chaos", 80)){
					bot.Player.Jump("r2", "Left");
					bot.Player.Kill("Chaos Gemrald");
					bot.Player.Jump("r3", "Left");
					bot.Player.Kill("Chaos Gemrald");
					bot.Player.Jump("r5", "Left");
					bot.Player.Kill("Frostwyrm Rider");
					bot.Player.Jump("r6", "Left");
					bot.Player.Kill("Water Draconian");
					bot.Player.Pickup("Fragment of Chaos");
					bot.Player.RejectExcept("Fragment of Chaos");
				}
			}
			
			bot.Player.Jump("Enter", "Spawn");
			
			if(!bot.Bank.Contains("Tendurrr the Assistant") && !bot.Inventory.Contains("Tendurrr the Assistant")){
				if(bot.Map.Name != "tercessuinotlim"){
					bot.Player.Join("citadel", "m22", "Right");
					bot.Sleep(2500);
					bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
					bot.Player.Jump("m2", "Center");
				}else
					bot.Player.Jump("m2", "Center");
				
				while(!bot.Inventory.Contains("Tendurrr the Assistant")){
					bot.Player.Kill("Dark Makai");
					bot.Player.Pickup("Tendurrr the Assistant", "Essence of Nulgath");
					bot.Player.RejectExcept("Tendurrr the Assistant", "Essence of Nulgath");
				}
				
				bot.Player.Jump("Enter", "Spawn");
			}else
				bot.Bank.ToInventory("Tendurrr the Assistant");
			
			if(bot.Map.Name != "evilwarnul")
				bot.Player.Join("evilwarnul", "r12", "Left");
			
			int counter = 0;
			while(!bot.Inventory.ContainsTempItem("Broken Betrayal Blade", 8)){
				bot.Player.Jump("r12", "Left");
				bot.Player.Kill("Legion Fenrir");
				bot.Player.Kill("Legion Fenrir");
				bot.Player.Jump("r13", "Left");
				bot.Player.Kill("Legion Fenrir");
				bot.Player.Kill("Legion Fenrir");
				bot.Player.Jump("r14", "Left");
				bot.Player.Kill("Legion Fenrir");
				if(counter >= 8){
					bot.Player.PickupFast("Archfiend's Favor", "Nulgath's Approval");
					counter = 0;
				}
				counter++;
			}
			
			bot.Player.RejectExcept("Archfiend's Favor", "Nulgath's Approval");
			
			bot.Player.Jump("Enter", "Spawn");
			bot.Quests.EnsureComplete(3743);
			
			bot.Wait.ForDrop("Blood Gem of the Archfiend");
			bot.Player.Pickup("Blood Gem of the Archfiend");
			bot.Player.RejectExcept("Blood Gem of the Archfiend", "Unidentified 10");
		}
	}
}