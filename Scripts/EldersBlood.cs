using RBot;
using System.Windows.Forms;

public class Script {
	
	public void ScriptMain(ScriptInterface bot){
		bot.Skills.StartTimer();
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Quests.Accept(802);
		if(bot.Quests.IsDailyComplete(802)){
			MessageBox.Show("Elder's blood daily quest already completed!");
			return;
		}
		
		bot.Inventory.BankAllCoinItems();
		
		bot.Player.Join("arcangrove", "LeftBack", "Left");
		
		bot.Quests.EnsureAccept(802);
		
		while(!bot.Inventory.ContainsTempItem("Slain Gorillaphant", 50))
			bot.Player.Kill("Gorillaphant");
		
		bot.Player.Jump("Enter", "Spawn");
		
		bot.Quests.EnsureComplete(802);
		
		bot.Wait.ForDrop("Elders' Blood");
		bot.Player.Pickup("Elders' Blood");
	}
}