using RBot;
using System.Windows.Forms;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Events.PlayerDeath += (a) => {
			bot.Schedule(11000, (b) => {
				bot.Player.Jump("Boss", "Left");
			});
		};
		bot.Skills.StartTimer();
		
		bot.Player.Join("escherion", "Boss", "Left");
		bot.Inventory.ToBank("Escherion's Helm");
		
		while(!bot.ShouldExit()){
			bot.Player.Kill("Escherion");
			bot.Bank.ToInventory("Escherion's Helm");
			bot.Player.Pickup("Escherion's Helm");
			bot.Player.RejectExcept("Escherion's Helm");
			bot.Inventory.ToBank("Escherion's Helm");
		}
	}
}
