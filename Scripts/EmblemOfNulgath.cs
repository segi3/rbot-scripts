using RBot;
using System.Windows.Forms;

public class Script {
		
	public void ScriptMain(ScriptInterface bot){
		bot.Skills.StartTimer();
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Inventory.BankAllCoinItems();
		
		if(!bot.Bank.Contains("Nation Round 4 Medal")){
			MessageBox.Show("You must have the Nation Round 4 Medal for this bot to work.");
			return;
		}
		
		bot.Sleep(1000);
		
		bot.Bank.ToInventory("Fiend Seal");
		bot.Bank.ToInventory("Nation Round 4 Medal");
		
		if(bot.Map.Name != "shadowblast")
			bot.Player.Join("shadowblast", "r13", "Left");
		else
			bot.Player.Jump("r13", "Left");
		
		while(!bot.ShouldExit()){
			bot.Quests.EnsureAccept(4748);
			
			bot.Player.Jump("r13", "Left");
			
			while(!bot.Inventory.Contains("Fiend Seal", 25) || !bot.Bank.Contains("Gem of Domination")){
				if(bot.Bank.Contains("Gem of Domination")){
					if(bot.Monsters.Exists("Legion Fenrir"))
						bot.Player.Kill("Legion Fenrir");
					else if(bot.Monsters.Exists("Legion Cannon"))
						bot.Player.Kill("Legion Cannon");
					else
						bot.Player.Kill("*");
				}else
					bot.Player.Kill("*");
				bool gemOfDom = bot.Player.DropExists("Gem of Domination");
				if(gemOfDom)
					bot.Bank.ToInventory("Gem of Domination");
				bot.Player.Pickup("Fiend Seal", "Gem of Domination");
				if(gemOfDom)
					bot.Inventory.ToBank("Gem of Domination");
				bot.Player.RejectExcept("Fiend Seal", "Gem of Domination");
			}
			
			bot.Player.Jump("Enter", "Spawn");
			
			bot.Bank.ToInventory("Gem of Domination");
			
			bot.Bank.ToInventory("Emblem of Nulgath");
			
			bot.Quests.EnsureComplete(4748);
			bot.Player.Pickup("Emblem of Nulgath");
			
			bot.Inventory.ToBank("Emblem of Nulgath");
			
			bot.Inventory.ToBank("Gem of Domination");
		}
	}
}