using RBot;
using System.Collections.Generic;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
	
		bot.Player.Join("cruxship");
	
		bot.Quests.EnsureAccept(4599);
		
		List<int> mapItems = MapItemIdentifier.GetMapItemsForQuest(4599);
		foreach(int id in mapItems){
			bot.Map.GetMapItem(id);
			bot.Sleep(1000);
		}
	}
}
