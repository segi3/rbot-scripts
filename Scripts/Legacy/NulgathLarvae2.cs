using RBot;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		bot.Skills.StartTimer();
		ManaGolem(bot, true);
	}
	
	public void ManaGolem(ScriptInterface bot, bool pureFarm = false){
        bot.Inventory.BankAllCoinItems();
        
        if(bot.Bank.FreeSlots > 0)
            bot.Inventory.ToBank("Voucher of Nulgath (non-mem)");
        
        bot.Bank.ToInventory("Gem of Nulgath");
        bot.Bank.ToInventory("Dark Crystal Shard");
        bot.Bank.ToInventory("Tainted Gem");
        bot.Bank.ToInventory("Totem of Nulgath");
        bot.Bank.ToInventory("Diamond of Nulgath");
        
        while(!bot.Bank.Contains("Unidentified 13") || pureFarm){
            while(!bot.Quests.IsInProgress(2566))
                bot.Quests.Accept(2566);
            
            if(!bot.Inventory.ContainsTempItem("Charged Mana Energy for Nulgath", 5)){
                bot.Player.Join("gilead", "r8", "Down");
                while(!bot.Inventory.ContainsTempItem("Charged Mana Energy for Nulgath", 5))
                    bot.Player.Kill("Mana Elemental");
            }
            
            if(!bot.Inventory.ContainsTempItem("Mana Energy for Nulgath", 1)){
                bot.Player.Join("elemental", "r5", "Down");
                while(!bot.Inventory.ContainsTempItem("Mana Energy for Nulgath", 1))
                    bot.Player.Kill("Mana Golem");
                bot.Player.RejectAll();
            }
            
            bot.Player.Jump("Enter", "Spawn");
            
            while(bot.Quests.IsInProgress(2566))
                bot.Quests.Complete(2566);
            
            bool u13 = bot.Player.DropExists("Unidentified 13");
            bool voucher = bot.Player.DropExists("Voucher of Nulgath (non-mem)");
            bool voucherMem = bot.Player.DropExists("Voucher of Nulgath");
            if(u13)
                bot.Bank.ToInventory("Unidentified 13");
            if(voucher)
                bot.Bank.ToInventory("Voucher of Nulgath (non-mem)");
            if(voucherMem)
            	bot.Bank.ToInventory("Voucher of Nulgath");
            bot.Player.Pickup("Unidentified 13", "Voucher of Nulgath (non-mem)", "Voucher of Nulgath", "Gem of Nulgath", "Dark Crystal Shard", "Tainted Gem", "Totem of Nulgath", "Unidentified 10", "Diamond of Nulgath");
            if(u13)
                bot.Inventory.ToBank("Unidentified 13");
            if(voucher && bot.Bank.FreeSlots > 0)
                bot.Inventory.ToBank("Voucher of Nulgath (non-mem)");
            if(voucherMem && bot.Bank.FreeSlots > 0)
            	bot.Inventory.ToBank("Voucher of Nulgath");
        }
    }
}
