using RBot;

public class Script {

	const string DROP_CHARGED = "Charged Mana Enery for Nulgath";
	const string DROP_ENERGY = "Mana Energy for Nulgath";
	const int QUEST_ID = 2566;

	const string ITEM_VOUCHER = "Voucher of Nulgath (non-mem)";
	const string ITEM_U13 = "Unidentified 13";

	public void ScriptMain(ScriptInterface bot){
		bot.Skills.Add(1, 2f);
		bot.Skills.Add(2, 0.8f);
		bot.Skills.Add(3, 0.65f);
		bot.Skills.Add(4, 2f);
		bot.Skills.StartTimer();
		
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Bank.FreeSlots > 0)
			bot.Inventory.ToBank(ITEM_VOUCHER);
		
		while(!bot.ShouldExit()){
			while(!bot.Quests.IsInProgress(QUEST_ID))
				bot.Quests.Accept(QUEST_ID);
			
			if(!bot.Inventory.ContainsTempItem(DROP_CHARGED, 5)){
				bot.Player.Join("gilead", "r8", "Down");
				while(!bot.Inventory.ContainsTempItem(DROP_CHARGED, 5))
					bot.Player.Kill("Mana Elemental");
			}
			if(!bot.Inventory.ContainsTempItem(DROP_ENERGY, 1)){
				bot.Player.Join("elemental", "r5", "Down");
				while(!bot.Inventory.ContainsTempItem(DROP_ENERGY, 1))
					bot.Player.Kill("Mana Golem");
				bot.Player.RejectAll();
			}
			bot.Player.Jump("Enter", "Spawn");
			bot.Quests.Complete(QUEST_ID);
			bool u13 = bot.Player.DropExists(ITEM_U13);
			bool voucher = bot.Player.DropExists(ITEM_VOUCHER);
			if(u13)
				bot.Bank.ToInventory(ITEM_U13);
			if(voucher)
				bot.Bank.ToInventory(ITEM_VOUCHER);
			bot.Player.Pickup(ITEM_U13, ITEM_VOUCHER, "Voucher of Nulgath", "Gem of Nulgath", "Dark Crystal Shard", "Tainted Gem", "Totem of Nulgath", "Unidentified 10", "Diamond of Nulgath");
			if(u13)
				bot.Inventory.ToBank(ITEM_U13);
			if(voucher && bot.Bank.FreeSlots > 0)
				bot.Inventory.ToBank(ITEM_VOUCHER);
		}
	}
}