using RBot;
using System.Windows.Forms;

/*---------------------- | Items Needed | ----------------------*\
 |																|
 |	Required:													|
 |	Hadean Onyx of Nulgath										|
 |	Voucher of Nulgath (non-mem)								|
 |																|
 |	x = Manual Acquisition May Be Necessary						|
 |  * = Bot Will Acquire										|
 |	Consumed (Sorted):           								|
 |	Unidentified 13 x 1          						  *		|
 |	Nulgath's Approval x 300    						  *		|
 |	Archfiend's Favor x 300     						  *		|
 |	Emblem of Nulgath x 20     							  *	    |
 |	Essence of Nulgath x 50     						  *		|
 |	Gem of Nulgath x 20 								 (x)	|
 |	Tainted Gem x 100     	        					  *		|
 |	Bone Dust x 20        	        					  *		|
 |	Elemental Ink x 10   	        					  *		|
 |	Elders' Blood x 1  	        						  *    	|
 |	The Secret 1 x 1  	        						  *    	|
 |	Black Knight Orb x 1        						  *   	|
 |	Aelita's Emerald x 1        						  x   	|
 |	Dwakel Decoder x 1	        						  x   	|
 |	Nulgath Shaped Chocolate x 1						  x   	|
 |																|
 |	Inventory Space Required (At Turn In): 15 (+3/4 Temp)   	|
 |	Max Space Required By Bot (At Any Instance): 7	     	    |
 |		- Assuming Unidentified 10 is already in inventory.     |
 |																|
\*--------------------------------------------------------------*/

public class Script{

    public void ScriptMain(ScriptInterface bot){
		bot.Skills.Add(2, 0.65f);
		bot.Skills.Add(3, new HealthUseRule(0.6f, 1f));
		bot.Skills.Add(4, 1f);
		bot.Skills.StartTimer();
        
        bot.Options.SafeTimings = true;
        bot.Options.RestPackets = true;

        bot.Player.LoadBank();

        bot.Inventory.BankAllCoinItems();
        
        ManaGolem(bot, true);

        if(!bot.Bank.Contains("Unidentified 13")){
            ManaGolem(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }

		//Quest has to be accepted to check whether it has been completed today.
		while(!bot.Quests.IsInProgress(802))
			bot.Quests.Accept(802);
		if(!bot.Quests.IsDailyComplete(802)){
            EldersBlood(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Bank.Contains("Archfiend's Favor", 300) || !bot.Inventory.Contains("Nulgath's Approval", 300)){
            Approvals(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Bank.Contains("Emblem of Nulgath", 20)){
            Emblems(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Bank.Contains("Essence of Nulgath", 50)){
            Essence(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Bank.Contains("Tainted Gem", 100)){
            TaintedGems(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Bank.Contains("Bone Dust", 20)){
            Bonedust(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Inventory.Contains("Elemental Ink", 10)){
            ElementalInk(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Inventory.Contains("The Secret 1")){
            TheSecret(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        if(!bot.Inventory.Contains("Black Knight Orb")){
            BlackKnightOrb(bot);
            bot.Player.Jump("Enter", "Spawn");
            bot.Sleep(1000);
        }
        
        ManaGolem(bot, true);
    }

    public void SellIfHas(ScriptInterface bot, string itemName){
        Item item = null;
        foreach(Item i in bot.Inventory.Items){
            if(i.Name.ToLower() == itemName.ToLower()){
                item = i;
                break;
            }
        }
        if (item != null)
            bot.Shops.SellItem(item.Name);
    }

    public void ManaGolem(ScriptInterface bot, bool pureFarm = false){
        bot.Inventory.BankAllCoinItems();
        
        if(bot.Bank.FreeSlots > 0)
            bot.Inventory.ToBank("Voucher of Nulgath (non-mem)");
        
        bot.Bank.ToInventory("Gem of Nulgath");
        bot.Bank.ToInventory("Dark Crystal Shard");
        bot.Bank.ToInventory("Tainted Gem");
        bot.Bank.ToInventory("Totem of Nulgath");
        bot.Bank.ToInventory("Diamond of Nulgath");
        
        while(!bot.Bank.Contains("Unidentified 13") || pureFarm){
            SellIfHas(bot, "Voucher of Nulgath");
            while(!bot.Quests.IsInProgress(2566))
                bot.Quests.Accept(2566);
            
            if(!bot.Inventory.ContainsTempItem("Charged Mana Energy for Nulgath", 5)){
                bot.Player.Join("gilead", "r8", "Down");
                while(!bot.Inventory.ContainsTempItem("Charged Mana Energy for Nulgath", 5))
                    bot.Player.Kill("Mana Elemental");
            }
            
            if(!bot.Inventory.ContainsTempItem("Mana Energy for Nulgath", 1)){
                bot.Player.Join("elemental", "r5", "Down");
                while(!bot.Inventory.ContainsTempItem("Mana Energy for Nulgath", 1))
                    bot.Player.Kill("Mana Golem");
                bot.Player.RejectAll();
            }
            
            bot.Player.Jump("Enter", "Spawn");
            
            while(bot.Quests.IsInProgress(2566))
                bot.Quests.Complete(2566);
            
            bool u13 = bot.Player.DropExists("Unidentified 13");
            bool voucher = bot.Player.DropExists("Voucher of Nulgath (non-mem)");
            if(u13)
                bot.Bank.ToInventory("Unidentified 13");
            if(voucher)
                bot.Bank.ToInventory("Voucher of Nulgath (non-mem)");
            bot.Player.Pickup("Unidentified 13", "Voucher of Nulgath (non-mem)", "Voucher of Nulgath", "Gem of Nulgath", "Dark Crystal Shard", "Tainted Gem", "Totem of Nulgath", "Unidentified 10", "Diamond of Nulgath");
            if(u13)
                bot.Inventory.ToBank("Unidentified 13");
            if(voucher && bot.Bank.FreeSlots > 0)
                bot.Inventory.ToBank("Voucher of Nulgath (non-mem)");
        }
    }

    public void Approvals(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();
        
        if(bot.Map.Name != "evilwarnul")
            bot.Player.Join("evilwarnul", "r2", "Up");
        
        bot.Bank.ToInventory("Archfiend's Favor");
        
        while(!bot.Inventory.Contains("Archfiend's Favor", 300) || !bot.Inventory.Contains("Nulgath's Approval", 300)){
            bot.Player.Kill("*");
            bot.Player.Pickup("Archfiend's Favor", "Nulgath's Approval");
            bot.Player.RejectExcept("Archfiend's Favor", "Nulgath's Approval");
        }
    }

    public void Emblems(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();
        
        bot.Bank.ToInventory("Fiend Seal");
        bot.Bank.ToInventory("Nation Round 4 Medal");
        
        if(bot.Map.Name != "shadowblast")
            bot.Player.Join("shadowblast", "r13", "Left");
        else
            bot.Player.Jump("r13", "Left");
        
        while(!bot.Bank.Contains("Emblem of Nulgath", 20)){
            while(!bot.Quests.IsInProgress(4748))
                bot.Quests.Accept(4748);
            
            bot.Player.Jump("r13", "Left");
            
            while(!bot.Inventory.Contains("Fiend Seal", 25) || !bot.Bank.Contains("Gem of Domination")){
                if(bot.Bank.Contains("Gem of Domination")){
                    if(bot.Monsters.Exists("Legion Fenrir"))
                        bot.Player.Kill("Legion Fenrir");
                    else if(bot.Monsters.Exists("Legion Cannon"))
                        bot.Player.Kill("Legion Cannon");
                    else
                        bot.Player.Kill("*");
                }else
                    bot.Player.Kill("*");
                bool gemOfDom = bot.Player.DropExists("Gem of Domination");
                if(gemOfDom)
                    bot.Bank.ToInventory("Gem of Domination");
                bot.Player.Pickup("Fiend Seal", "Gem of Domination");
                if(gemOfDom)
                    bot.Inventory.ToBank("Gem of Domination");
                bot.Player.RejectExcept("Fiend Seal", "Gem of Domination");
            }
            
            bot.Player.Jump("Enter", "Spawn");
            
            bot.Bank.ToInventory("Gem of Domination");
            bot.Bank.ToInventory("Emblem of Nulgath");
            
            while(bot.Quests.IsInProgress(4748))
                bot.Quests.Complete(4748);
            bot.Player.Pickup("Emblem of Nulgath");
            
            bot.Inventory.ToBank("Emblem of Nulgath");
            bot.Inventory.ToBank("Gem of Domination");
        }
    }

    public void Essence(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();
        
        bot.Player.Join("citadel", "m22", "Right");
        bot.Sleep(5000);
        bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
        bot.Player.Jump("m2", "Center");
        
        while(!bot.Bank.Contains("Essence of Nulgath", 50)){
            bot.Player.Kill("Dark Makai");
            if(bot.Player.DropExists("Essence of Nulgath")){
                bot.Bank.ToInventory("Essence of Nulgath");
                bot.Player.Pickup("Essence of Nulgath");
                bot.Inventory.ToBank("Essence of Nulgath");
            }
            if(bot.Player.DropExists("Tendurrr the Assistant")){
                bot.Bank.ToInventory("Tendurrr the Assistant");
                bot.Player.Pickup("Tendurrr the Assistant");
                bot.Inventory.ToBank("Tendurrr the Assistant");
            }
            if(bot.Player.Cell != "m2")
                bot.Player.Jump("m2", "Center");
            bot.Player.RejectExcept("Essence of Nulgath", "Tendurrr the Assistant");
        }
    }

    public void TaintedGems(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();
        
        if(bot.Map.Name != "battleunderb")
            bot.Player.Join("battleunderb");
        
        bot.Bank.ToInventory("Bone Dust");
        
        int forceKill = 0;
        while(!bot.Bank.Contains("Tainted Gem", 100)){
            while(!bot.Quests.IsInProgress(568))
                bot.Quests.Accept(568);

            int cKill = 0;
            int counter = 0;
            while(!bot.Inventory.Contains("Bone Dust", 30) || cKill < forceKill){
                bot.Player.Kill("Skeleton Warrior");
                if(counter >= 5){
                    bot.Player.Pickup("Bone Dust", "Tainted Gem");
                    bot.Player.RejectExcept("Bone Dust", "Tainted Gem");
                    counter = 0;
                }
                counter++;
                cKill++;
    		}
            bot.Player.Jump("Enter", "Spawn");
            
            int turnInTries = 0;
            while(bot.Quests.IsInProgress(568) && turnInTries < 2){
                bot.Quests.Complete(568);
                turnInTries++;
            }

            if(bot.Quests.IsInProgress(568)){
                forceKill = 10;
            }
            
            bot.Bank.ToInventory("Tainted Gem");
            bot.Player.Pickup("Bone Dust", "Tainted Gem");
            bot.Player.RejectExcept("Bone Dust", "Tainted Gem");
            bot.Inventory.ToBank("Tainted Gem");
        }
    }

    public void Bonedust(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();
        
        if(bot.Map.Name != "battleunderb")
            bot.Player.Join("battleunderb");
        
        bot.Bank.ToInventory("Bone Dust");
        
        while(!bot.Inventory.Contains("Bone Dust", 20)){
            bot.Player.Kill("Skeleton Warrior");
            bot.Player.Pickup("Bone Dust");
            bot.Player.RejectExcept("Bone Dust");
        }
    }

    public void ElementalInk(ScriptInterface bot){
        if(bot.Inventory.Contains("Elemental Ink", 10))
            return;
        bot.Inventory.BankAllCoinItems();
        
        if(!bot.Inventory.Contains("Mystic Quills", 4)){
            bot.Player.Join("mobius", "Slugfit", "Spawn");
            while(!bot.Inventory.Contains("Mystic Quills", 4)){
                bot.Player.Kill("*");
                bot.Player.Pickup("Mystic Quills");
                bot.Player.RejectExcept("Mystic Quills");
            }
            bot.Player.Jump("Enter", "Spawn");
        }
        
        bot.Player.Join("spellcraft");

        bot.SendPacket("%xt%zm%buyItem%671975%13284%549%1637%");
        bot.Sleep(2500);
        bot.SendPacket("%xt%zm%buyItem%671975%13284%549%1637%");
        bot.Sleep(2500);
    }

    public void EldersBlood(ScriptInterface bot){
        if(bot.Bank.Contains("Elders' Blood") || bot.Inventory.Contains("Elders' Blood"))
            return;
        
        bot.Inventory.BankAllCoinItems();
        
        bot.Player.Join("arcangrove", "LeftBack", "Left");
        
        while(!bot.Quests.IsInProgress(802))
            bot.Quests.Accept(802);
        
        while(!bot.Inventory.ContainsTempItem("Slain Gorillaphant", 50))
            bot.Player.Kill("Gorillaphant");
        
        bot.Player.Jump("Enter", "Spawn");
        
        while(bot.Quests.IsInProgress(802))
            bot.Quests.Complete(802);
        
        bot.Wait.ForDrop("Elders' Blood");
        bot.Player.Pickup("Elders' Blood");
    }

    public void TheSecret(ScriptInterface bot){
        if(!bot.Inventory.Contains("The Secret 1")){
            bot.Player.Join("willowcreek");
            bot.Player.Jump("Yard2", "Right");
            while(!bot.Quests.IsInProgress(623))
                bot.Quests.Accept(623);
            while(!bot.Inventory.Contains("The Secret 1")){
                bot.Player.Kill("Hidden Spy");
                bot.Player.Pickup("The Secret 1");
            }
        }
    }

    public void BlackKnightOrb(ScriptInterface bot){
        bot.Inventory.BankAllCoinItems();

        if(bot.Inventory.Contains("Black Knight Orb"))
            return;
        
        while(!bot.Quests.IsInProgress(318))
            bot.Quests.Accept(318);
        
        bot.Player.Join("well", "Boss", "Spawn");
        while(!bot.Inventory.ContainsTempItem("Black Knight Leg Piece"))
            bot.Player.Kill("Gell oh no");
        
        bot.Player.Join("greendragon", "Boss", "Spawn");
        while(!bot.Inventory.ContainsTempItem("Black Knight Chest Piece"))
            bot.Player.Kill("Greenguard Dragon");
        
        bot.Player.Join("deathgazer", "Enter", "Spawn");
        while(!bot.Inventory.ContainsTempItem("Black Knight Shoulder Piece"))
            bot.Player.Kill("Deathgazer");
        
        bot.Player.Join("trunk", "Enter", "Spawn");
        while(!bot.Inventory.ContainsTempItem("Black Knight Arm Piece"))
            bot.Player.Kill("Greenguard Basilisk");
        
        while(bot.Quests.IsInProgress(318))
            bot.Quests.Complete(318);
        
        bot.Wait.ForDrop("Black Knight Orb");
        bot.Player.Pickup("Black Knight Orb");
    }
}
