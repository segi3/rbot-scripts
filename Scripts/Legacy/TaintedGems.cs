using RBot;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Skills.StartTimer();
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Player.LoadBank();
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
		
		bot.Bank.ToInventory("Bone Dust");

		TaintedGems(bot, 200);
		return;
		
		while(!bot.ShouldExit()){
			while(!bot.Quests.IsInProgress(568))
				bot.Quests.Accept(568);
			while(!bot.Inventory.Contains("Bone Dust", 25)){
				bot.Player.Kill("Skeleton Warrior");
				bot.Player.Pickup("Bone Dust", "Tainted Gem");
				bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
			}
			bot.Player.Jump("Enter", "Spawn");
			while(bot.Quests.IsInProgress(568))
				bot.Quests.Complete(568);
			bot.Bank.ToInventory("Tainted Gem");
			bot.Player.Pickup("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Inventory.ToBank("Tainted Gem");
		}
	}

	public void TaintedGems(ScriptInterface bot, int count){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
		
		bot.Bank.ToInventory("Bone Dust");
		
		int forceKill = 0;
		while(!bot.Bank.Contains("Tainted Gem", count)){
			while(!bot.Quests.IsInProgress(568))
				bot.Quests.Accept(568);

			int cKill = 0;
			int counter = 0;
			while(!bot.Inventory.Contains("Bone Dust", 30) || cKill < forceKill){
				bot.Player.Kill("Skeleton Warrior");
				if(counter >= 5){
					bot.Player.Pickup("Bone Dust", "Tainted Gem", "Undead Essence");
					bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
					counter = 0;
				}
				counter++;
				cKill++;
			}
			bot.Player.Jump("Enter", "Spawn");
			
			int turnInTries = 0;
			while(bot.Quests.IsInProgress(568) && turnInTries < 2){
				bot.Quests.Complete(568);
				turnInTries++;
			}

			if(bot.Quests.IsInProgress(568)){
				forceKill = 10;
			}
			
			bot.Bank.ToInventory("Tainted Gem");
			bot.Player.Pickup("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Inventory.ToBank("Tainted Gem");
		}
	}
}