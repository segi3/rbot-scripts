using System;
using RBot;

public class Script{

    public void ScriptMain(ScriptInterface bot){
		bot.Skills.StartTimer();
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Events.PlayerDeath += (a) => {
			bot.Schedule(11000, (b) => {
				bot.Player.Jump("Enter", "Spawn");
				ScriptManager.RestartScript();
			});
		};
		
		bot.Player.LoadBank();
		bot.Inventory.BankAllCoinItems();
		
		if(!bot.Bank.Contains("Unidentified 13")){
			Larvae(bot, false);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Inventory.Contains("Hadean Onyx of Nulgath")){
			bot.Player.Join("citadel", "m22", "Right");
			bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
			bot.Player.Jump("m4", "Center");
		
			bot.Player.KillForItem("Shadow of Nulgath", "Hadean Onyx of Nulgath", 1);
			
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		bot.Quests.EnsureAccept(802);
		if(!bot.Quests.IsDailyComplete(802) && !bot.Inventory.Contains("Elder's Blood", 3)){
			EldersBlood(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Bank.Contains("Archfiend's Favor", 300) || !bot.Bank.Contains("Nulgath's Approval", 300)){
			Approvals(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Bank.Contains("Emblem of Nulgath", 20)){
			Emblems(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Bank.Contains("Essence of Nulgath", 50)){
			Essence(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Bank.Contains("Tainted Gem", 100)){
			TaintedGems(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Bank.Contains("Bone Dust", 20)){
			Bonedust(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Inventory.Contains("Elemental Ink", 10)){
			ElementalInk(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Inventory.Contains("The Secret 1")){
			TheSecret(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
		
		if(!bot.Inventory.Contains("Black Knight Orb")){
			BlackKnightOrb(bot);
			bot.Player.Jump("Enter", "Spawn");
			bot.Sleep(1000);
		}
	}
	
	public void BlackKnightOrb(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		bot.Quests.EnsureAccept(318);
		
		bot.Player.Join("well", "Boss", "Spawn");
		bot.Player.KillForItem("Gell oh no", "Black Knight Leg Piece", 1, true);
		
		bot.Player.Join("greendragon", "Boss", "Spawn");
		bot.Player.KillForItem("Greenguard Dragon", "Black Knight Chest Piece", 1, true);
		
		bot.Player.Join("deathgazer", "Enter", "Spawn");
		bot.Player.KillForItem("Deathgazer", "Black Knight Shoulder Piece", 1, true);
		
		bot.Player.Join("trunk", "Enter", "Spawn");
		bot.Player.KillForItem("Greenguard Basilisk", "Black Knight Arm Piece", 1, true);
		
		bot.Quests.EnsureComplete(318);
		
		bot.Wait.ForDrop("Black Knight Orb");
		bot.Player.Pickup("Black Knight Orb");
	}
	
	public void TheSecret(ScriptInterface bot){
		bot.Player.Join("willowcreek");
		bot.Player.Jump("Yard2", "Right");
		bot.Quests.EnsureAccept(623);
		bot.Player.KillForItem("Hidden Spy", "The Secret 1", 1);
	}
	
	public void ElementalInk(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		if(!bot.Inventory.Contains("Mystic Quills", 4)){
			bot.Player.Join("mobius", "Slugfit", "Spawn");
			bot.Player.KillForItem("*", "Mystic Quills", 4);
			bot.Player.Jump("Enter", "Spawn");
		}
		
		bot.Player.Join("spellcraft");
		
		bot.SendPacket("%xt%zm%buyItem%671975%13284%549%1637%");
        bot.Sleep(2500);
        bot.SendPacket("%xt%zm%buyItem%671975%13284%549%1637%");
        bot.Sleep(2500);
	}
	
	public void Bonedust(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
			
		bot.Bank.ToInventory("Bone Dust");
		bot.Player.KillForItem("Skeleton Warrior", "Bone Dust", 20);
	}
	
	public void TaintedGems(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
		
		bot.Bank.ToInventory("Bone Dust");
		
		int forceExtra = 0;
		while(!bot.Bank.Contains("Tainted Gem", 100)){
			bot.Quests.EnsureAccept(568);
		
			bot.Player.KillForItem("Skeleton Warrior", "Bone Dust", 30 + forceExtra);
			
			bot.Player.Jump("Enter", "Spawn");
			if(!bot.Quests.EnsureComplete(568, -1, false, 4))
				forceExtra += 5;
			else
				forceExtra = 0;
				
			bot.Wait.ForDrop("Tainted Gem");
			bot.Bank.StackGlitch("Tainted Gem");
			bot.Player.RejectExcept("Tainted Gem", "Bone Dust");
		}
	}
	
	public void Essence(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		bot.Player.Join("citadel", "m22", "Right");
        bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
       	bot.Player.Jump("m2", "Center");
       	
       	int room = 1;
       	while(!bot.Bank.Contains("Essence of Nulgath", 50)){
       		bot.Player.Kill("Dark Makai");
       		bot.Player.Kill("Dark Makai");
       		bot.Bank.StackGlitch("Essence of Nulgath");
       		bot.Bank.StackGlitch("Tendurrr the Assistant");
       		bot.Player.RejectExcept("Essence of Nulgath", "Tendurrr the Assistant");
       		bot.Player.Jump("m" + room, "Center");
       		room++;
       		if(room >= 3)
       			room = 1;
       	}
	}
	
	public void Emblems(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		bot.Bank.ToInventory("Fiend Seal");
		bot.Bank.ToInventory("Nation Round 4 Medal");
		
		if(bot.Map.Name != "shadowblast")
			bot.Player.Join("shadowblast", "r13", "Left");
		else
			bot.Player.Jump("r13", "Left");
			
		while(!bot.Bank.Contains("Emblem of Nulgath", 20)){
			bot.Quests.EnsureAccept(4748);
			
			while(!bot.Inventory.Contains("Fiend Seal", 25) || !bot.Bank.Contains("Gem of Domination")){
				if(bot.Bank.Contains("Gem of Domination")){
					if(bot.Monsters.Exists("Legion Fenrir"))
						bot.Player.Kill("Legion Fenrir");
					else if(bot.Monsters.Exists("Legion Cannon"))
						bot.Player.Kill("Legion Cannon");
					else
						bot.Player.Kill("*");
				}else
					bot.Player.Kill("*");
				bot.Bank.StackGlitch("Gem of Domination");
				bot.Player.Pickup("Fiend Seal");
				bot.Player.RejectExcept("Fiend Seal", "Gem of Domination");
			}
			
			bot.Player.Jump("Enter", "Spawn");
			
			bot.Bank.ToInventory("Gem of Domination");
			bot.Bank.ToInventory("Emblem of Nulgath");
			
			bot.Quests.EnsureComplete(4748);
			
			bot.Wait.ForDrop("Emblem of Nulgath");
			bot.Player.Pickup("Emblem of Nulgath");
		}
	}
	
	public void Approvals(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "evilwarnul")
			bot.Player.Join("evilwarnul", "r2", "Up");
		
		bot.Bank.ToInventory("Archfiend's Favor");
		
		bot.Player.KillForItems("*", new string[] { "Archfiend's Favor", "Nulgath's Approval" }, new int[] { 300, 300 });
	}
	
	public void EldersBlood(ScriptInterface bot){
		bot.Inventory.BankAllCoinItems();
		bot.Quests.EnsureAccept(802);
		
		bot.Player.Join("arcangrove", "LeftBack", "Left");
		bot.Player.KillForItem("Gorillaphant", "Slain Gorillaphant", 50, true);
		
		bot.Player.Jump("Enter", "Spawn");
		bot.Quests.EnsureComplete(802);
		
		bot.Wait.ForDrop("Elder's Blood");
		bot.Player.Pickup("Elder's Blood");
	}
	
	public void Larvae(ScriptInterface bot, bool farm){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Bank.FreeSlots > 0)
			bot.Inventory.ToBank("Voucher of Nulgath (non-mem)");
		
		bot.Bank.ToInventory("Gem of Nulgath");
		bot.Bank.ToInventory("Dark Crystal Shard");
		bot.Bank.ToInventory("Tainted Gem");
		bot.Bank.ToInventory("Totem of Nulgath");
		bot.Bank.ToInventory("Diamong of Nulgath");
		
		while(!bot.Bank.Contains("Unidentified 13") || farm){
			if(bot.Inventory.Contains("Voucher of Nulgath"))
                bot.Shops.SellItem("Voucher of Nulgath");
            
			bot.Quests.EnsureAccept(2566);
			
			if(!bot.Inventory.ContainsTempItem("Charged Mana Energy for Nulgath", 5)){
				bot.Player.Join("gilead", "r8", "Down");
				bot.Player.KillForItem("Mana Elemental", "Chaged Mana Energy for Nulgath", 5, true);
			}
			
			if(!bot.Inventory.ContainsTempItem("Mana Energy for Nulgath")){
				bot.Player.Join("elemental", "r5", "Down");
				bot.Player.KillForItem("Mana Golem", "Mana Energy for Nulgath", 1, true);
			}
			
			bot.Player.Jump("Enter", "Spawn");
			
			bot.Quests.EnsureComplete(2566);
			
			bot.Bank.StackGlitch("Unidentified 13");
			bot.Bank.StackGlitch("Voucher of Nulgath (non-mem)");
			bot.Player.Pickup("Unidentified 13", "Voucher of Nulgath (non-mem)", "Vocuher of Nulgath", "Gem of Nulgath", "Dark Crystal Shard", "Tainted Gem", "Totem of Nulgath", "Unidentified 10", "Diamond of Nulgath");
		}
    }
}
