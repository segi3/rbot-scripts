using RBot;

public class Script {

	public void ScriptMain(ScriptInterface bot){
		bot.Skills.StartTimer();
		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;
		
		bot.Player.LoadBank();
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
		
		bot.Bank.ToInventory("Bone Dust");

		TaintedGems(bot, 200);
		return;
		
		while(!bot.ShouldExit()){
			while(!bot.Quests.IsInProgress(568))
				bot.Quests.Accept(568);
			while(!bot.Inventory.Contains("Bone Dust", 25)){
				bot.Player.Kill("Skeleton Warrior");
				bot.Player.Pickup("Bone Dust", "Tainted Gem");
				bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
			}
			bot.Player.Jump("Enter", "Spawn");
			while(bot.Quests.IsInProgress(568))
				bot.Quests.Complete(568);
			bot.Bank.ToInventory("Tainted Gem");
			bot.Player.Pickup("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Player.RejectExcept("Bone Dust", "Tainted Gem", "Undead Essence");
			bot.Inventory.ToBank("Tainted Gem");
		}
	}

	public void TaintedGems(ScriptInterface bot, int count){
		bot.Inventory.BankAllCoinItems();
		
		if(bot.Map.Name != "battleunderb")
			bot.Player.Join("battleunderb");
		
		bot.Bank.ToInventory("Bone Dust");
		
		int forceExtra = 0;
		while(!bot.Bank.Contains("Tainted Gem", count)){
			bot.Quests.EnsureAccept(568);
		
			bot.Player.KillForItem("Skeleton Warrior", "Bone Dust", 30 + forceExtra);
			
			bot.Player.Jump("Enter", "Spawn");
			if(!bot.Quests.EnsureComplete(568, -1, false, 4))
				forceExtra += 5;
			else
				forceExtra = 0;
				
			bot.Wait.ForDrop("Tainted Gem");
			bot.Bank.StackGlitch("Tainted Gem");
			bot.Player.RejectExcept("Tainted Gem", "Bone Dust");
		}
	}
}