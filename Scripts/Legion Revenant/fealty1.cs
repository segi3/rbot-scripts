using RBot;
using System;

// quest 6897
// Revenant's Spellscroll , 20

// Ultra Aeacus , /revenant
// Aeacus Empowered , 50

// Forgotten Soul , /revenant
// Tethered Soul , 300

// Pure Shadowscythe | Shadow Warrior | Shadow Guardian , /shadowrealmpast
// Darkened Essence , 500

// 5 head dracolich , /necrodungeon
// Dracolich Contract , 1000

// timekeeper 62123
// ssg 45363
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Aeacus Empowered", "Tethered Soul", "Darkened Essence", "Dracolich Contract", "Revenant's Spellscroll"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        while(!bot.Inventory.Contains("Revenant's Spellscroll", 20)) {

           //if (!bot.Quests.IsInProgress(6897))
                bot.Quests.EnsureAccept(6897);

            if (!bot.Inventory.Contains("Aeacus Empowered", 50)) {
                getAeacus(bot, 50);
            }
            if (!bot.Inventory.Contains("Tethered Soul", 300)) {
                getTetheredSoul(bot, 300);
            }
            if (!bot.Inventory.Contains("Darkened Essence", 500)) {
                getDarkenedEssence(bot, 500);
            }
            if (!bot.Inventory.Contains("Dracolich Contract", 1000)) {
                getDracolichContract(bot, 1000);
            }

            bot.Sleep(2000);
            //if (bot.Quests.CanComplete(6897)) 
                bot.Quests.EnsureComplete(6897);
                bot.Sleep(2000);
            
        }
        
	}

    public void getAeacus(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Aeacus Empowered", quantity)) return;

        if (bot.Map.Name != "revenant")
            bot.Player.Join("revenant-1e99", "r3", "Left");
        if (bot.Player.Cell != "r3")
            bot.Player.Jump("r3", "Left");
        
        // bot.Player.SetSpawnPoint("r3", "Left");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/timekeeper.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(62123);

        while(!bot.Inventory.Contains("Aeacus Empowered", quantity) && !bot.ShouldExit()) {
            bot.Sleep(1000);
            if (bot.Monsters.Exists("Ultra Aeacus"))
                bot.Player.Kill("Ultra Aeacus");
        }
        return;
    }

    public void getTetheredSoul(ScriptInterface bot, int quantity) {
        if (bot.Inventory.Contains("Tethered Soul", quantity)) return;

        if (bot.Map.Name != "revenant")
            bot.Player.Join("revenant-1e99", "r2", "Left");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Left");

        // bot.Player.SetSpawnPoint("Enter", "Spawn");

        // bot.Skills.StartSkills("Skills/timekeeper.xml");
		// bot.Player.EquipItem(62123);
        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        // bot.Skills.StartSkills("Skills/ShadowSyctheGeneralFarm.xml");
		// bot.Player.EquipItem(45363);

        while(!bot.Inventory.Contains("Tethered Soul", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;

    }

    public void getDarkenedEssence(ScriptInterface bot, int quantity) {
        if (bot.Inventory.Contains("Darkened Essence", quantity)) return;

        if (bot.Map.Name != "shadowrealmpast")
            bot.Player.Join("shadowrealmpast-1e99", "Enter", "Spawn");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Spawn");

        // bot.Player.SetSpawnPoint("r3", "Left");

        // bot.Skills.StartSkills("Skills/timekeeper.xml");
		// bot.Player.EquipItem(62123);

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        // bot.Skills.StartSkills("Skills/ShadowSyctheGeneralFarm.xml");
		// bot.Player.EquipItem(45363);

        while(!bot.Inventory.Contains("Darkened Essence", quantity) && !bot.ShouldExit()) {

            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;

    }

    public void getDracolichContract(ScriptInterface bot, int quantity) {
        if (bot.Inventory.Contains("Dracolich Contract", quantity)) return;

        if (bot.Map.Name != "necrodungeon")
            bot.Player.Join("necrodungeon-1e99", "r22", "Down");
        if (bot.Player.Cell != "r22")
            bot.Player.Jump("r22", "Down");
        
        // room sebeum bos r21 Left

        // bot.Player.SetSpawnPoint("r22", "Down");

        // bot.Skills.StartSkills("Skills/timekeeper.xml");
		// bot.Player.EquipItem(62123);

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        // bot.Skills.StartSkills("Skills/ShadowSyctheGeneralFarm.xml");
		// bot.Player.EquipItem(45363);

        while(!bot.Inventory.Contains("Dracolich Contract", quantity) && !bot.ShouldExit()) {

            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;

    }
}