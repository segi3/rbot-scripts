using RBot;
using System;

// quest 4743
// Emblem of Dage , 10

// vhl 38259
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Diamond Token of Dage", "Defeated Makai", "Legion Token"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        while(!bot.Inventory.Contains("Diamond Token of Dage", 300)) {

            bot.Quests.EnsureAccept(4743);

            // makai
            if (!bot.Inventory.Contains("Defeated Makai", 25)) {

                bot.Sleep(1000);
                bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
                bot.Sleep(1000);
                bot.Player.EquipItem(45831);

                if(bot.Map.Name != "tercessuinotlim") {
					bot.Player.Join("citadel", "m22", "Right");
					bot.Sleep(2500);
					bot.Player.Join("tercessuinotlim", "Enter", "Spawn");
					bot.Player.Jump("m2", "Center");
				} else {
                    bot.Player.Jump("m2", "Center");
                }
                while (!bot.Inventory.Contains("Defeated Makai", 25)) {
                    bot.Player.Attack("*");
			        bot.Sleep(200);
                }
                bot.Player.Jump("Enter", "Spawn");
            }

            bot.Sleep(1000);
            bot.Skills.StartSkills("Skills/VoidHighlordDef.xml");
            bot.Sleep(1000);
            bot.Player.EquipItem(38259);

            resforms(bot, 6000);

            // carnax, Carnax Eye
            if (!bot.Inventory.ContainsTempItem("Carnax Eye")) {
                if (bot.Map.Name != "aqlesson")
                    bot.Player.Join("aqlesson-1e99", "Frame9", "Right");
                if (bot.Player.Cell != "Frame9")
                    bot.Player.Jump("Frame9", "Right");
                bot.Player.Kill("Carnax");
			    bot.Sleep(200);
            }
            // while(!bot.Inventory.ContainsTempItem("Carnax Eye")) {
            //     bot.Player.Kill("Carnax");
			//     bot.Sleep(200);
            // }

            resforms(bot, 6000);

            // kathool, Kathool Tentacle
            if (!bot.Inventory.ContainsTempItem("Kathool Tentacle")) {
                if (bot.Map.Name != "deepchaos")
                    bot.Player.Join("deepchaos-1e99", "Frame4", "Left");
                if (bot.Player.Cell != "Frame4")
                    bot.Player.Jump("Frame4", "Left");
                bot.Player.Kill("Kathool");
			    bot.Sleep(200);
            }
            // while(!bot.Inventory.ContainsTempItem("Kathool Tentacle")) {
            //     bot.Player.Kill("Kathool");
			//     bot.Sleep(200);
            // }

            resforms(bot, 6000);

            // red dragon, Red Dragon's Fang
            if (!bot.Inventory.ContainsTempItem("Red Dragon's Fang")) {
                if (bot.Map.Name != "lair")
                    bot.Player.Join("lair-1e99", "End", "Center");
                if (bot.Player.Cell != "End")
                    bot.Player.Jump("End", "Center");
                bot.Player.Kill("Red Dragon");
			    bot.Sleep(200);
            }
            // while(!bot.Inventory.ContainsTempItem("Red Dragon's Fang")) {
            //     bot.Player.Kill("Red Dragon");
			//     bot.Sleep(200);
            // }

            resforms(bot, 6000);

            // fluffy, Fluffy's Bones
            if (!bot.Inventory.ContainsTempItem("Fluffy's Bones")) {
                if (bot.Map.Name != "dflesson")
                    bot.Player.Join("dflesson-1e99", "r12", "Right");
                if (bot.Player.Cell != "r12")
                    bot.Player.Jump("r12", "Right");
                bot.Player.Kill("Fluffy The Dracolich");
			    bot.Sleep(200);
            }
            // while(!bot.Inventory.ContainsTempItem("Fluffy's Bones")) {
            //     bot.Player.Kill("Fluffy The Dracolich");
			//     bot.Sleep(200);
            // }

            // bot.Skills.StartSkills("Skills/VoidHighlordDef.xml");
            resforms(bot, 6000);

            // blood titan, Blood Titan's Blade
            if (!bot.Inventory.ContainsTempItem("Blood Titan's Blade")) {
                if (bot.Map.Name != "bloodtitan")
                    bot.Player.Join("bloodtitan-1e99", "Enter", "Spawn");
                if (bot.Player.Cell != "Enter")
                    bot.Player.Jump("Enter", "Spawn");
                bot.Player.Kill("Blood Titan");
			    bot.Sleep(200);
            }
            // while(!bot.Inventory.ContainsTempItem("Blood Titan's Blade")) {
            //     bot.Player.Kill("Blood Titan");
			//     bot.Sleep(200);
            // }

            resforms(bot, 6000);

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(4743)) 
                bot.Quests.EnsureComplete(4743);
                bot.Sleep(2000);
        }
        
	}

    
    public void resforms(ScriptInterface bot, int mlisec) {
        bot.Player.Rest();
        bot.Sleep(mlisec);
    }
}