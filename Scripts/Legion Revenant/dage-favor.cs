using RBot;
using System;

// vhl 38259
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Dage's Favor"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "underworld")
            bot.Player.Join("underworld-1e99", "r8", "Left");
        if (bot.Player.Cell != "r8")
            bot.Player.Jump("r8", "Left");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        while(!bot.Inventory.Contains("Dage's Favor", 5000)) {

            bot.Player.Attack("*");
            bot.Sleep(200);
            
        }
        
	}
}