using RBot;
using System;

// quest 4743
// seraphic medals, 6248
// mega seraphic medals, 6249

// vhl 38259
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Dark Token"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "seraphicwardage")
            bot.Player.Join("seraphicwardage-1e99", "Enter", "Spawn");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Spawn");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        while(!bot.Inventory.Contains("Dark Token", 400)) {

            bot.Quests.EnsureAccept(6248);
            bot.Quests.EnsureAccept(6249);

            while (!bot.Inventory.ContainsTempItem("Seraphic Medals", 50)) {
                bot.Player.Attack("*");
			    bot.Sleep(200);
            }

            bot.Sleep(2000);
            while (bot.Quests.CanComplete(6248))  {
                bot.Quests.EnsureComplete(6248);
                bot.Sleep(2000);
                bot.Quests.EnsureAccept(6248);
                bot.Sleep(2000);
            }
            while (bot.Quests.CanComplete(6249))  {
                bot.Quests.EnsureComplete(6249);
                bot.Sleep(2000);
                bot.Quests.EnsureAccept(6249);
                bot.Sleep(2000);
            }
            bot.Sleep(5000);
        }
        
	}
}