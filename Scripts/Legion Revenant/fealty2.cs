using RBot;
using System;

// quest 6898
// Conquest Wreath , 6

// any , /doomvault, [r1, Left]
// Grim Cohort Conquered , 500

// any , /mummies, [Enter, Spawn]
// Ancient Cohort Conquered , 500

// any , /wrath, [r4, Right]
// Pirate Cohort Conquered , 500

// any , /doomhaven, [r16, Down]
// Battleon Cohort Conquered , 500

// any , /overworld, [r2, Down]
// Mirror Cohort Conquered , 500

// any , /deathpits, [r3, Left]
// Darkblood Cohort Conquered , 500

// any , /maxius, [r2, Right]
// Vampire Cohort Conquered , 500

// any , /curseshore, [Enter, RIght]
// Spirit Cohort Conquered , 500

// any , /dragonbone, [r2, Right]
// Dragon Cohort Conquered , 500

// any , /doomwood, [r2, Up]
// Doomwood Cohort Conquered , 500

// timekeeper 62123
// ssg 45363
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Conquest Wreath",
        "Grim Cohort Conquered", "Ancient Cohort Conquered", "Pirate Cohort Conquered",
        "Battleon Cohort Conquered", "Mirror Cohort Conquered", "Darkblood Cohort Conquered",
        "Vampire Cohort Conquered", "Spirit Cohort Conquered", "Dragon Cohort Conquered",
        "Doomwood Cohort Conquered"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        while(!bot.Inventory.Contains("Conquest Wreath", 6)) {

           if (!bot.Quests.IsInProgress(6898))
                bot.Quests.EnsureAccept(6898);

           grim(bot, 500);
           ancient(bot, 500);
           pirate(bot, 500);
           battleon(bot, 500);
           mirror(bot, 500);
           darkblood(bot, 500);
           vampire(bot, 500);
           spirit(bot, 500);
           dragon(bot, 500);
           doomwood(bot, 500);

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(6898)) 
                bot.Quests.EnsureComplete(6898);
                bot.Sleep(2000);
            
        }
        
	}

    public void grim(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Grim Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "doomvault")
            bot.Player.Join("doomvault-1e99", "r1", "Left");
        if (bot.Player.Cell != "r1")
            bot.Player.Jump("r1", "Left");

        while(!bot.Inventory.Contains("Grim Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void ancient(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Ancient Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "mummies")
            bot.Player.Join("mummies-1e99", "Enter", "Spawn");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Spawn");

        while(!bot.Inventory.Contains("Ancient Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void pirate(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Pirate Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "wrath")
            bot.Player.Join("wrath-1e99", "r4", "Right");
        if (bot.Player.Cell != "r4")
            bot.Player.Jump("r4", "Right");

        while(!bot.Inventory.Contains("Pirate Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void battleon(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Battleon Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "doomhaven")
            bot.Player.Join("doomhaven-1e99", "r16", "Down");
        if (bot.Player.Cell != "r16")
            bot.Player.Jump("r16", "Down");

        while(!bot.Inventory.Contains("Battleon Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void mirror(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Mirror Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "overworld")
            bot.Player.Join("overworld-1e99", "r2", "Down");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Down");

        while(!bot.Inventory.Contains("Mirror Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void darkblood(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Darkblood Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "deathpits")
            bot.Player.Join("deathpits-1e99", "r3", "Left");
        if (bot.Player.Cell != "r3")
            bot.Player.Jump("r3", "Left");

        while(!bot.Inventory.Contains("Darkblood Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void vampire(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Vampire Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "maxius")
            bot.Player.Join("maxius-1e99", "r2", "Right");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Right");

        while(!bot.Inventory.Contains("Vampire Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void spirit(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Spirit Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "curseshore")
            bot.Player.Join("curseshore-1e99", "Enter", "Right");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Right");

        while(!bot.Inventory.Contains("Spirit Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void dragon(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Dragon Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "dragonbone")
            bot.Player.Join("dragonbone-1e99", "r2", "Right");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Right");

        while(!bot.Inventory.Contains("Dragon Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }

    public void doomwood(ScriptInterface bot, int quantity){ 
        if (bot.Inventory.Contains("Doomwood Cohort Conquered", quantity)) return;

        if (bot.Map.Name != "doomwood")
            bot.Player.Join("doomwood-1e99", "r2", "Up");
        if (bot.Player.Cell != "r2")
            bot.Player.Jump("r2", "Up");

        while(!bot.Inventory.Contains("Doomwood Cohort Conquered", quantity) && !bot.ShouldExit()) {
            bot.Player.Attack("*");
			bot.Sleep(200);
        }
        return;
    }
}