using RBot;
using System;

// quest 4742
// Emblem of Dage , 10

// timekeeper 62123
// ssg 45363
// abbysal angel 45831

public class Script{

    public static readonly string [] DROPS = {
        "Emblem of Dage", "Legion Seal", "Gem of Mastery"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.DisableFX = true;
        bot.Options.HuntDelay = 1500;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "shadowblast")
            bot.Player.Join("shadowblast-1e99", "r10", "Left");
        if (bot.Player.Cell != "r10")
            bot.Player.Jump("r10", "Left");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/AbyysalAngelShadow.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(45831);

        while(!bot.Inventory.Contains("Emblem of Dage", 10)) {

            bot.Quests.EnsureAccept(4742);

            bot.Player.Attack("*");
			bot.Sleep(200);

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(4742)) 
                bot.Quests.EnsureComplete(4742);
                bot.Sleep(2000);
            
        }
        
	}
}