using RBot;
using System;

// quest 7978
// Souls of Limbo, 25
// Essence of Luxuria, 1
// Essence of Gluttony, 1
// Essence of Avarice, 1

// vhl 38259
// abbysal angel 45831
// revenant 48571

public class Script{

    public static readonly string [] DROPS = {
        "Indulgence"
    };

	public void ScriptMain(ScriptInterface bot){

        bot.Events.PlayerDeath += (a) => {
            bot.Schedule(11000, (b) => {
                // bot.Player.Jump("Enter", "Spawn");
                ScriptManager.RestartScript();
            });
        };
        
        bot.Sleep(2000);

		bot.Options.SafeTimings = true;
		bot.Options.RestPackets = true;

        bot.Options.ExitCombatBeforeQuest = true;
        bot.Options.PrivateRooms = true;

        foreach (string s in DROPS) {
            bot.Drops.Add(s);
        }
        bot.Drops.RejectElse = true;
        bot.Drops.Start();

        if (bot.Map.Name != "sevencircles")
            bot.Player.Join("sevencircles-1e99", "Enter", "Right");
        if (bot.Player.Cell != "Enter")
            bot.Player.Jump("Enter", "Right");

        bot.Sleep(1000);
        bot.Skills.StartSkills("Skills/LegionRevenant.xml");
        bot.Sleep(1000);
		bot.Player.EquipItem(48571);

        while(!bot.Inventory.Contains("Indulgence", 60)) {

            bot.Quests.EnsureAccept(7978);

            if (!bot.Inventory.ContainsTempItem("Souls of Limbo", 25)) {
                bot.Player.Jump("r2", "Right");
                while (!bot.Inventory.ContainsTempItem("Souls of Limbo", 25)) {
                    bot.Player.Attack("*");
                    bot.Sleep(200);
                }
            }
           
            if (!bot.Inventory.ContainsTempItem("Essence of Luxuria", 1)) {
                bot.Player.Jump("r3", "Right");
                while (!bot.Inventory.ContainsTempItem("Essence of Luxuria", 1)) {
                    bot.Player.Attack("*");
                    bot.Sleep(200);
                }
            }
            
            if (!bot.Inventory.ContainsTempItem("Essence of Gluttony", 1)) {
                bot.Player.Jump("r5", "Right");
                while (!bot.Inventory.ContainsTempItem("Essence of Gluttony", 1)) {
                    bot.Player.Attack("*");
                    bot.Sleep(200);
                }
            }
            
            if (!bot.Inventory.ContainsTempItem("Essence of Avarice", 1)) {
                bot.Player.Jump("r7", "Right");
                while (!bot.Inventory.ContainsTempItem("Essence of Avarice", 1)) {
                    bot.Player.Attack("*");
                    bot.Sleep(200);
                }
            }
            

            bot.Sleep(2000);
            if (bot.Quests.CanComplete(7978))  {
                bot.Quests.EnsureComplete(7978);
                bot.Sleep(2000);
            }
        }
        
	}
}